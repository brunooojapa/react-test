// @flow
import React, { Component } from "react";
import { Link } from "react-router";
import "./index.sass";

class Nav extends Component<Props, States> {
  render() {
    return (
      <div className="Nav">
        <div className="Nav__wrapper">
          <Link to={"/beers"}>Home >> Beer</Link>{" "}
        </div>
      </div>
    );
  }
}

type Props = {};

type States = {};

export default Nav;
