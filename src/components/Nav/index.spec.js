import React from "react";
import Nav from "../../components/Nav";

const wrapper = <Nav />;

describe("A suite for Nav component", async () => {
  it("should be selectable by class", async () => {
    expect(shallow(wrapper).is(".Nav")).toBe(true);
  });

  it("should mount in a full DOM", async () => {
    expect(mount(wrapper).find(".Nav").length).toBe(1);
  });
});
