// @flow
import React, { Component } from "react";
import Icon from "../../components/Icon";
import Search from "../../components/Search";
import Login from "../../components/Login";
import "./index.sass";

class Header extends Component<Props, States> {
  render() {
    return (
      <header className="Header">
        <section className="container">
          <div className="Header__Wrapper">
            <Icon className="Header__logo" name="logo" />
            <Search className="Header__search" />
            <Login className="Header__login" />
          </div>
        </section>
      </header>
    );
  }
}

type Props = {};

type States = {};

export default Header;
