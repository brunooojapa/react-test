import React from 'react'
import Header from '../../components/Header'

const wrapper = <Header/>

describe('A suite for Header component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Header')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Header').length).toBe(1)
	})
})
