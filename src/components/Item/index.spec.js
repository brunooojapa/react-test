import React from 'react'
import Item from '../../components/Item'

const wrapper = <Item/>

describe('A suite for Item component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Item')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Item').length).toBe(1)
	})
})
