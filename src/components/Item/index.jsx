// @flow
import React, { Component } from "react";
import { Link } from "react-router";
import Image from "../../components/Image";
import type BeerType from "../../types/BeerType";
import "./index.sass";

class Item extends Component<Props, States> {
  render() {
    var { props: { data } } = this;

    if (!data) return "No data!";

    return (
      <li className="Item">
        <div className="Item__wrapper">
          <Link to={`beers/${data.id}`}>
            <Image
              className="Item__image"
              src={data.image_url}
              alt={data.name}
            />
            <h3 className="Item__title">{data.name}</h3>
            <p className="Item__tagline">{data.tagline}</p>
            <p className="Item__attenuation">
              Attenuation Level: {data.attenuation_level}
            </p>
            <span className="Item__details">More info</span>
          </Link>
        </div>
      </li>
    );
  }
}

type Props = {
  data: BeerType
};

type States = {};

export default Item;
