import React from "react";
import Login from "../../components/Login";

const wrapper = <Login />;

describe("A suite for Login component", async () => {
  it("should be selectable by class", async () => {
    expect(shallow(wrapper).is(".Login")).toBe(true);
  });

  it("should mount in a full DOM", async () => {
    expect(mount(wrapper).find(".Login").length).toBe(1);
  });
});
