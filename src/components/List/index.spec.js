import React from 'react'
import List from '../../components/List'

const wrapper = <List/>

describe('A suite for List component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.List')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.List').length).toBe(1)
	})
})
