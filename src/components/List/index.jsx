// @flow
import React, { Component } from "react";
import Item from "../../components/Item";
import type BeerType from "../../types/BeerType";
import "./index.sass";

class List extends Component<Props, States> {
  render() {
    var { props: { data } } = this;

    if (!data) return "No data!";

    return <ul className="List">{data.map(beer => <Item data={beer} />)}</ul>;
  }
}

type Props = {
  data: Array<BeerType>
};

type States = {};

export default List;
