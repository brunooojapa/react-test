// @flow
import React, { Component } from "react";
import logo from "../../assets/logo.png";
import "./index.sass";

class Icon extends Component<Props, States> {
  getIcon = (name: String) => {
    switch (name) {
      case "logo":
        return logo;
    }
  };

  render() {
    var { props: { className, name }, props, getIcon } = this;

    return (
      <img
        {...props}
        className={className ? className + " Icon" : "Icon"}
        alt={name}
        src={getIcon(name)}
      />
    );
  }
}

type Props = {
  name: String
};

type States = {};

export default Icon;
