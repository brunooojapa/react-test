import React from 'react'
import Icon from '../../components/Icon'

const wrapper = <Icon/>

describe('A suite for Icon component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Icon')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Icon').length).toBe(1)
	})
})
