import React from 'react'
import Black from '../../components/Black'

const wrapper = <Black/>

describe('A suite for Black component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Black')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Black').length).toBe(1)
	})
})
