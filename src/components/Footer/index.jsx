// @flow
import React, { Component } from 'react'
import './index.sass'

class Footer extends Component<Props, States> {
  getYear = (date: Date) => {
  	return date.getFullYear()
  }

  render() {
    var { getYear } = this

    return (
      <footer className="Footer">
        <section className="container">
          Copyright @ {getYear(new Date())}
        </section>
      </footer>
    )
  }
}

type Props = {
}

type States = {
}

export default Footer
