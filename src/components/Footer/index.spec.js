import React from 'react'
import Footer from '../../components/Footer'

const wrapper = <Footer/>

describe('A suite for Footer component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Footer')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Footer').length).toBe(1)
	})
})
