import React from 'react'
import Image from '../../components/Image'

const wrapper = <Image/>

describe('A suite for Image component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Image')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Image').length).toBe(1)
	})
})
