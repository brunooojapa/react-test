// @flow
import React, { Component } from 'react'
import './index.sass'

class Image extends Component<Props, States> {
  render() {
    var { props: { className, src, alt }, props } = this

    return (
      <img
        {...props}
        className={className ? className + ' Image' : 'Image'}
        alt={alt ? alt : 'Alternative text'}/>
    )
  }
}

type Props = {
  name: String
}

type States = {
}

export default Image
