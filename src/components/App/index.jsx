// @flow
import React, { Component } from 'react'
import { Router, Route, Link, browserHistory } from 'react-router'
import Header from '../../components/Header'
import Main from '../../components/Main'
import Footer from '../../components/Footer'
import Home from '../../containers/Home'
import NotFound from '../../containers/NotFound'
import Beers from '../../containers/Beers'
import Beer from '../../containers/Beer'
import './index.sass'

class App extends Component<Props, States> {
  render() {
    return (
      <div className="App">
        <Header/>
        <Main>
          <Router history={browserHistory}>
            <Route exact path='/' component={Home}/>
            <Route exact path='/beers' component={Beers}/>
            <Route exact path='/beers/:beerId' component={Beer}/>
            <Route path='*' component={NotFound}/>
          </Router>
        </Main>
        <Footer/>
      </div>
    )
  }
}

type Props = {
}

type States = {
}

export default App
