// @flow
import React, { Component } from 'react'
import './index.sass'

class Main extends Component<Props, States> {
  render() {
    var { props: { children } } = this

    return (
      <main className="Main">
        <section className="container">
          {children}
        </section>
      </main>
    )
  }
}

type Props = {
  children: Object
}

type States = {
}

export default Main
