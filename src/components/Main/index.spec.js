import React from 'react'
import Main from '../../components/Main'

const wrapper = <Main/>

describe('A suite for Main component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Main')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Main').length).toBe(1)
	})
})
