import React from "react";
import Banner from "../../components/Banner";

const wrapper = <Banner />;

describe("A suite for Banner component", async () => {
  it("should be selectable by class", async () => {
    expect(shallow(wrapper).is(".Banner")).toBe(true);
  });

  it("should mount in a full DOM", async () => {
    expect(mount(wrapper).find(".Banner").length).toBe(1);
  });
});
