// @flow
import React, { Component } from "react";
import Image from "../../components/Image";
import type BeerType from "../../types/BeerType";
import "./index.sass";

class Item extends Component<Props, States> {
  render() {
    var { props: { data } } = this;

    if (!data) return "No data!";

    return (
      <div className="Card">
        <div className="Card__image">
          <Image src={data.image_url} alt={data.name} />
        </div>
        <div className="Card__text">
          <h2>{data.name}</h2>
          <small>{data.tagline}</small>
          <p>{data.description}</p>
          <p>
            <b>Attenuation level:</b> {data.attenuation_level}
          </p>
          <p>
            <b>Brewers tips:</b> <br /> {data.brewers_tips}
          </p>
          <p>
            <b>Price:</b> $ {data.ph}0
          </p>
          <a className="Card__btn" href="#">
            Buy Beer
          </a>
        </div>
      </div>
    );
  }
}

type Props = {
  data: BeerType
};

type States = {};

export default Item;
