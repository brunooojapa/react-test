import React from 'react'
import Card from '../../components/Card'

const wrapper = <Card/>

describe('A suite for Card component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Card')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Card').length).toBe(1)
	})
})
