// @flow
import React, { Component } from "react";
import "./index.sass";

class Search extends Component<Props, States> {
  render() {
    return (
      <div className="Search">
        <input className="Search__input" type="text" />
        <button className="Search__btn">Search</button>
      </div>
    );
  }
}

type Props = {};

type States = {};

export default Search;
