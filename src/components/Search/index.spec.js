import React from "react";
import Search from "../../components/Search";

const wrapper = <Search />;

describe("A suite for Search component", async () => {
  it("should be selectable by class", async () => {
    expect(shallow(wrapper).is(".Search")).toBe(true);
  });

  it("should mount in a full DOM", async () => {
    expect(mount(wrapper).find(".Search").length).toBe(1);
  });
});
