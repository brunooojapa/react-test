import React from 'react'
import Home from '../../components/Home'

const wrapper = <Home/>

describe('A suite for Home component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Home')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Home').length).toBe(1)
	})
})
