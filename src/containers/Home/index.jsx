// @flow
import React, { Component } from "react";
import { Link } from "react-router";
import Banner from "../../components/Banner";
import "./index.sass";

class Home extends Component<Props, States> {
  render() {
    return (
      <div className="Home">
        <Banner />
        <div className="Alert">
          <p className="Alert__title">Hey! Are you over 18?</p>
          <div className="Alert__wrapper">
            <Link to={"/home"}>No</Link> <Link to={"/beers"}>Yes</Link>{" "}
          </div>
        </div>
      </div>
    );
  }
}

type Props = {};

type States = {};

export default Home;
