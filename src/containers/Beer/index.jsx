// @flow
import React, { Component } from "react";
import Card from "../../components/Card";
import Banner from "../../components/Banner";
import Nav from "../../components/Nav";

import API from "../../middlewares/API";
import type BeerType from "../../types/BeerType";
import "./index.sass";

class Beer extends Component<Props, States> {
  state = {
    beer: {}
  };

  componentWillMount() {
    const url = "https://api.punkapi.com/v2/beers/2";

    API.get(url)
      .then(beer => beer[0])
      .then(beer => this.setState({ beer }));
  }

  render() {
    var { state: { beer } } = this;

    return (
      <div className="Beer">
        <Banner />
        <Nav />
        <Card data={beer} />
      </div>
    );
  }
}

type Props = {};

type States = {
  beer: BeerType
};

export default Beer;
