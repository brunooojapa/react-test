import React from 'react'
import Beer from '../../components/Beer'

const wrapper = <Beer/>

describe('A suite for Beer component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Beer')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Beer').length).toBe(1)
	})
})
