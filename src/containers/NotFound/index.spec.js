import React from 'react'
import NotFound from '../../components/NotFound'

const wrapper = <NotFound/>

describe('A suite for NotFound component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.NotFound')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.NotFound').length).toBe(1)
	})
})
