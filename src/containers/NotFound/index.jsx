// @flow
import React, { Component } from "react";
import "./index.sass";

class NotFound extends Component<Props, States> {
  render() {
    return <div className="NotFound">Ops! Not Found :(</div>;
  }
}

type Props = {};

type States = {};

export default NotFound;
