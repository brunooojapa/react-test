import React from 'react'
import Beers from '../../components/Beers'

const wrapper = <Beers/>

describe('A suite for Beers component', async () => {
	it('should be selectable by class', async () => {
		expect(shallow(wrapper).is('.Beers')).toBe(true)
	})

	it('should mount in a full DOM', async () => {
		expect(mount(wrapper).find('.Beers').length).toBe(1)
	})
})
