// @flow
import React, { Component } from "react";
import List from "../../components/List";
import Banner from "../../components/Banner";
import API from "../../middlewares/API";
import type BeerType from "../../types/BeerType";
import "./index.sass";

class Beers extends Component<Props, States> {
  state = {
    beers: []
  };

  componentWillMount() {
    const url = "https://api.punkapi.com/v2/beers";

    API.get(url).then(beers => this.setState({ beers }));
  }

  render() {
    var { state: { beers } } = this;

    return (
      <div className="Beers">
        <Banner />
        <h2>Beer Festival</h2>
        <List data={beers} />
      </div>
    );
  }
}

type Props = {};

type States = {
  state: Array<BeerType>
};

export default Beers;
