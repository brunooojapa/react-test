// @flow
type BeerType = {
  id: Number,
  name: String,
  tagline: String,
  description: String,
  image_url: String
}

export default BeerType
