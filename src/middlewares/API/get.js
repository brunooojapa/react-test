// @flow
const get = (url: String) => {
  return new Promise((resolve, reject) => {
    if (!url) reject('No URL!')

    fetch(url).then(response => resolve(response.json()))
  })
}

export default get
