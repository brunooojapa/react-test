// @flow
import React from 'react'
import { render } from 'react-dom'
import 'milligram'
// import registerServiceWorker from './registerServiceWorker'
import App from './components/App'
import './index.sass'

render(
  <App/>,
  document.getElementById('root')
)
// registerServiceWorker()
